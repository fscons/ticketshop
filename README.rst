========================
fscons-ticketshop
========================

A webapp to sell tickets for the FSCONS conference

Installation of Dependencies
=============================

    $ pip install -r ticketshop/requirements.txt
