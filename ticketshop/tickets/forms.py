import django.forms as forms
from django.core.exceptions import ValidationError
from .models import TicketPurchase, Ticket, TicketType

class TicketPurchaseForm(forms.ModelForm):
    class Meta:
        model = TicketPurchase
        fields = [
                'contact_first_name', 'contact_surname', 'contact_email',
                'contact_subscribe', 'additional_information' ]

        widgets = {
          'contact_first_name': forms.TextInput(attrs={'class':'form-control'}),
          'contact_surname': forms.TextInput(attrs={'class':'form-control'}),
          'contact_email': forms.TextInput(attrs={'class':'form-control'}),
          'additional_information': forms.Textarea(attrs={'class':'form-control', 'rows': 2}),
          }


class TicketForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        """
        We extends the __init__ method to be able to restrict
        ticket types to available types by overwriting the queryset
        """
        super(TicketForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Ticket
        fields = [
                'name_on_badge',
                'gender',
                'returning_visitor',
        ]
        widgets = {
            'name_on_badge': forms.TextInput( attrs = {
                    'class':'form-control', 'placeholder': 'Name on badge'}),
            'gender': forms.Select( attrs={'class':'form-control'} ),
            'returning_visitor': forms.Select( attrs={'class':'form-control'} ),
        }


class BaseTicketFormSet(forms.formsets.BaseFormSet):
    def __init__(self, *args, **kargs):
        self.instances = kargs['instances']
        del kargs['instances']
        super(BaseTicketFormSet, self).__init__(*args, **kargs)

    def _initial_form_count(self):
        return len(self.instances)

    def _construct_form(self, i, **kargs):
        kargs['instance'] = self.instances[i]
        kargs['empty_permitted'] = False
        return super(BaseTicketFormSet, self)._construct_form(i, **kargs)


TicketFormSet = forms.formsets.formset_factory(
        TicketForm,
        formset=BaseTicketFormSet,
        can_delete=False
)


class TicketSelectionForm(forms.Form):
    """
    This is the first form: it shows all ticket types and allows the user
    to select how many ticket of each type are desired.
    """

    def __init__(self, ticket_types, *args, **kargs):
        """
        We extends the init method with a new parameter,
        ticket_types, which is the list of ticket_types to show in the form
        """
        super(TicketSelectionForm, self).__init__(*args, **kargs)
        self.ticket_types = ticket_types
        for type_ in ticket_types:
            if type_.available():
                field = forms.IntegerField(min_value=0, max_value=10)
                self.fields['quantity:%s' % type_.id] = field

    def clean(self):
        """ This checks that at least one ticket has been selected """
        data = super(TicketSelectionForm, self).clean()
        total = 0
        for k in data:
            total += data[k]
        if total == 0:
            raise ValidationError("You need to select at least one ticket")
        return data


class TicketsForm(forms.Form):
    """
    This is the first form: it shows all ticket types and allows the user
    to select how many ticket of each type are desired.
    """

    def __init__(self, ticket_types, valid_coupon_codes, *args, **kargs):
        """
        We extends the init method with two new parameters:
        - ticket_types is the list of ticket_types to show in the form
        - valid_coupon_codes is a list of valid coupon codes used in validation
        """
        super(TicketsForm, self).__init__(*args, **kargs)
        self.ticket_types = ticket_types
        for type_ in ticket_types:
            if type_.available():
                field = forms.IntegerField(
                        min_value=0,
                        max_value=10
                )
                self.fields['quantity:%s' % type_.id] = field
