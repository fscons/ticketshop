from datetime import date, datetime, timedelta
from random import randint


def daterange(begin, end):
    """
    Returns an iterator of all dates between begin and end (included).
    """
    if isinstance(begin, datetime):
        d = begin.date()
    elif isinstance(begin, date):
        d = begin
    else:
        assert False, "Unknown format for begin"
    if isinstance(end, datetime):
        end = end.date()
    elif isinstance(begin, date):
        pass
    else:
        assert False, "Unknown format for end"
    while d <= end:
        yield d
        d += timedelta(days=1)


def checksum(number):
    """
    Compute a checksum for a given number according to the Luhn algorithm
    https://en.wikipedia.org/wiki/Luhn_algorithm
    """
    digits = [int(d) for d in str(number)]
    digits.reverse()
    for i in range(0, len(digits), 2):
        double = digits[i]*2
        digits[i] = double if double < 10 else double - 9
    return sum(digits) * 9 % 10


def randrefnumber():
    """
    Generate a random reference number
    """
    n = randint(10000000, 99999999)
    return n*100 + checksum(n)
