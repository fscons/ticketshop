from datetime import datetime, timedelta

from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404, render

from .forms import TicketSelectionForm, TicketPurchaseForm, TicketForm
from .mails import send_booking_confirmation_email
from .models import TicketPurchase, TicketType, Ticket, PreBooking
from .utils import randrefnumber


def index(request):
    """
    Index view: shows a form with a list of ticket that one can buy
    """
    ticket_types = TicketType.objects.all()
    form = TicketSelectionForm(ticket_types, request.POST or None)
    if request.session.session_key is None:
        request.session.create()

    if form.is_valid():
        quantities = form.cleaned_data
        PreBooking.objects.filter(session=request.session.session_key).delete()
        prebooking_expiration = (
            datetime.now() + timedelta(minutes=settings.PREBOOKING_TIMEOUT))
        for ticket_type in ticket_types:
            PreBooking.objects.create(
                session=request.session.session_key,
                ticket_type=ticket_type,
                quantity=quantities['quantity:{0.id}'.format(ticket_type)],
                expiration_date=prebooking_expiration)
        return redirect('details', randrefnumber())

    return render(request, "index.html", dict(form=form))


def details(request, ref):
    """
    Once the user has selected what tickets they wants to buy,
    this view will let them enter the details.
    """
    sid = request.session.session_key
    if sid is None or not PreBooking.objects.filter(session=sid).exists():
        messages.warning(request, "Your session has expired")
    pre_bookings = PreBooking.objects.filter(
        session=sid, expiration_date__gt=datetime.now())
    if not pre_bookings.exists():
        messages.warning(request, "Your registration has expired")
        return redirect('index')
    purchase_form = TicketPurchaseForm(request.POST or None)
    ticket_forms = []
    for pre_booking in pre_bookings:
        for _ in range(pre_booking.quantity):
            ticket_forms.append(
                TicketForm(
                    request.POST or None,
                    prefix="ticket%i" % len(ticket_forms),
                    instance=Ticket(ticket_type=pre_booking.ticket_type)))
    if purchase_form.is_valid() and all(f.is_valid() for f in ticket_forms):
        purchase = purchase_form.instance
        purchase.ref = ref
        purchase.save()
        for f in ticket_forms:
            f.instance.purchase = purchase
            f.instance.save()
        pre_bookings.delete()
        send_booking_confirmation_email(purchase)
        return redirect('confirm', purchase.id)
    return render(
        request,
        "details.html",
        dict(
            purchase_form=purchase_form,
            ticket_forms=ticket_forms,
            expiration_date=pre_bookings[0].expiration_date,
            PREBOOKING_TIMEOUT=settings.PREBOOKING_TIMEOUT,
            ))


def edit_registration(request, uuid):
    """
    A view that allows the attendee to edit their registration.
    """
    purchase = get_object_or_404(TicketPurchase, pk=uuid)
    purchase_form = TicketPurchaseForm(request.POST or None, instance=purchase)
    ticket_forms = []
    for i, ticket in enumerate(purchase.tickets.all()):
        ticket_forms.append(TicketForm(request.POST or None,
                                       prefix="ticket%i" % i,
                                       instance=ticket))
    if purchase_form.is_valid() and all(f.is_valid() for f in ticket_forms):
        purchase_form.instance.save()
        for form in ticket_forms:
            form.instance.save()
        return redirect('confirm', purchase.id)
    return render(request, "details.html",
                  dict(purchase_form=purchase_form, ticket_forms=ticket_forms))


def cancel_registration(request, uuid):
    """
    A view allowing the user to cancel their registration.
    When called with GET, it shows a confirmation dialog.
    The user confirms using POST.
    A paid registration cannot be cancelled this way.
    """
    purchase = get_object_or_404(TicketPurchase, pk=uuid)
    if purchase.paid:
        messages.warning(
            request,
            "This purchase has already been paid. "
            "If you still want to cancel it, please contact us at "
            "<a href='mailto:info@fscons.org'>info@fscons.org</a>")
        return redirect('confirm', purchase.id)
    if purchase.cancelled:
        messages.warning(request, "Purchase already cancelled")
        return redirect('confirm', purchase.id)
    if request.POST:
        action = request.POST.get('action', None)
        if action == 'confirm':
            purchase.cancel(by="USER")
            purchase.save()
            messages.success(request, "Registration successfully cancelled")
        return redirect('confirm', purchase.id)
    return render(request, "cancellation.html", dict(registration=purchase))


def confirmation(request, uuid):
    """ Show a summary of the purchase and the payment information """
    purchase = get_object_or_404(TicketPurchase, pk=uuid)
    return render(request, "confirmation.html", {'purchase': purchase})
