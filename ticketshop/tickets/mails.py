"""
Using this file to add signal handlers because it is loaded automatically by
django
"""
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import get_template


def send_booking_confirmation_email(purchase):
    body = get_template('mails/booking_confirmation.txt')

    send_mail(
        subject='Registration confirmation',
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[purchase.get_contact_mailbox()],
        message=body.render({'purchase': purchase}),
        fail_silently=False)


def send_payment_confirmation_email(purchase):
    assert purchase.paid
    body = get_template('mails/confirmation.txt')

    send_mail(
        subject='Payment confirmation',
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[purchase.get_contact_mailbox()],
        message=body.render({'purchase': purchase}),
        fail_silently=False)


def send_cancellation_email(purchase):
    assert purchase.cancelled and purchase.cancelled_by == "USER"
    body = get_template('mails/user_cancellation.txt')

    send_mail(
        subject='Registration cancelled',
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[purchase.get_contact_mailbox()],
        message=body.render({'purchase': purchase}),
        fail_silently=False)
