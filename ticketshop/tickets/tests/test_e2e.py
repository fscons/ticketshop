from datetime import datetime, timedelta

from django.core.urlresolvers import reverse
from django.test import LiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.ui import Select

from tickets.models import TicketPurchase, TicketType


def setUpModule():
    global DRIVER
    DRIVER = WebDriver()


def tearDownModule():
    DRIVER.quit()


class SeleniumTestCase(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(SeleniumTestCase, cls).setUpClass()
        cls.selenium = DRIVER

    @classmethod
    def tearDownClass(cls):
        super(SeleniumTestCase, cls).tearDownClass()

    def get(self, *args, **kwargs):
        self.selenium.get(self.live_server_url + reverse(*args, **kwargs))


class RegistrationTests(SeleniumTestCase):

    def setUp(self):
        TicketType.objects.create(name="Regular", price="1024", id=1,
                                  sales_end=datetime.now()+timedelta(days=7))

    def test_register(self):
        self.get('index')
        quantity = Select(self.selenium.find_element_by_name('quantity:1'))
        quantity.select_by_visible_text("2")
        self.selenium.find_element_by_id('btn-register').click()

        (self.selenium.find_element_by_name("contact_first_name")
            .send_keys("John"))
        (self.selenium.find_element_by_name("contact_surname")
            .send_keys("Doe"))
        (self.selenium.find_element_by_name("contact_email")
            .send_keys("john@example.se"))
        (self.selenium.find_element_by_name("ticket0-name_on_badge")
            .send_keys("Jonny Hacker"))
        (Select(self.selenium.find_element_by_name("ticket0-gender"))
            .select_by_visible_text("Trans*"))
        (Select(
            self.selenium.find_element_by_name("ticket0-returning_visitor"))
            .select_by_visible_text("Yes"))
        (self.selenium.find_element_by_name("ticket1-name_on_badge")
            .send_keys("Mystery guest"))
        self.selenium.find_element_by_id('btn-checkout').click()

        self.assertIn(
            "John Doe",
            self.selenium.find_element_by_id("contact-name").text)
        self.assertIn(
            "john@example.se",
            self.selenium.find_element_by_id("contact-email").text)
        self.assertIn(
            "Jonny Hacker",
            self.selenium.find_element_by_id("ticket1-name_on_badge").text)
        self.assertIn(
            "Trans*",
            self.selenium.find_element_by_id("ticket1-gender").text)
        self.assertIn(
            "Yes",
            self.selenium.find_element_by_id("ticket1-returning_visitor").text)
        self.assertIn(
            "Mystery guest",
            self.selenium.find_element_by_id("ticket2-name_on_badge").text)
        self.assertIn(
            "unspecified",
            self.selenium.find_element_by_id("ticket2-gender").text)
        self.assertIn(
            "unspecified",
            self.selenium.find_element_by_id("ticket2-returning_visitor").text)


class EditTests(SeleniumTestCase):

    def setUp(self):
        TicketType.objects.create(name="Regular", price="1024",
                                  sales_end=datetime.now()+timedelta(days=7))
        self.reg = TicketPurchase.objects.create(
            contact_first_name="Tryphon",
            contact_surname="Tournesol",
            contact_email="tournesol@moulinsart.be",
        )
        self.reg.tickets.create(name_on_badge="Pr. Tournesol")

    def test_edit_name_on_badge(self):
        new_name_on_badge = "Professor Tournesol"
        self.get('confirm', args=(self.reg.id,))
        self.selenium.find_element_by_id('btn-edit').click()
        (self.selenium.find_element_by_name("ticket0-name_on_badge")
            .send_keys(new_name_on_badge))
        self.selenium.find_element_by_id("btn-checkout").click()
        self.assertIn(
            new_name_on_badge,
            self.selenium.find_element_by_id("ticket1-name_on_badge").text)


class CancellationTests(SeleniumTestCase):

    def setUp(self):
        TicketType.objects.create(name="Regular", price="1024",
                                  sales_end=datetime.now()+timedelta(days=7))
        self.reg = TicketPurchase.objects.create(
            contact_first_name="Tryphon",
            contact_surname="Tournesol",
            contact_email="tournesol@moulinsart.be",
        )
        self.reg.tickets.create(name_on_badge="Pr. Tournesol")

    def test_cancel_registration(self):
        self.get('cancel', args=(self.reg.pk,))
        assert ("Confirmation required"
                in self.selenium.find_element_by_tag_name('h2').text)
        self.selenium.find_element_by_id('btn-confirm').click()
        assert TicketPurchase.objects.get(pk=self.reg.pk).cancelled

    def test_dont_cancel_registration(self):
        self.get('cancel', args=(self.reg.pk,))
        assert ("Confirmation required"
                in self.selenium.find_element_by_tag_name('h2').text)
        self.selenium.find_element_by_id('btn-cancel').click()
        assert not TicketPurchase.objects.get(pk=self.reg.pk).cancelled

    def test_cant_cancel_paid_registration(self):
        self.reg.mark_as_paid()
        self.get('cancel', args=(self.reg.pk,))
        self.assertTrue(
            self.selenium.current_url.endswith(
                reverse('confirm', args=(self.reg.id,))))
