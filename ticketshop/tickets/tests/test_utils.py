from datetime import datetime, date
from unittest import TestCase
from ..utils import daterange, checksum


class CheckSumTest(TestCase):
    """
    Test the function that generates a checksum according to the Luhn algorithm
    """

    def testZero(self):
        self.assertEqual(0, checksum(0))

    def testWikipediaExample(self):
        self.assertEqual(3, checksum(7992739871))

    def testExample1(self):
        self.assertEqual(0, checksum(109763550))

    def testExample2(self):
        self.assertEqual(7, checksum(111044060))


class DaterangeTest(TestCase):
    """
    Tests the daterange function
    """
    def testOneDayRange(self):
        """
        Test a range from one day to the same.
        """
        day = datetime.now()
        self.assertEqual([day.date()], list(daterange(day,day)))

    def testTwoDays(self):
        day1 = date(2013,01,03)
        day2 = date(2013,01,04)
        self.assertEqual([day1,day2], list(daterange(day1, day2)))

    def testMore(self):
        days = [ date(2013,01,d) for d in range(4,19) ]
        self.assertEqual( days, list(daterange(days[0], days[-1])) )
