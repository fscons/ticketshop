from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import Client
from django.test import TestCase

from ..models import PreBooking
from .utilities import mkTicketType, mkTicketPurchase


class TestCreateRegistration(TestCase):

    def test_no_prebooking(self):
        """
        If there is no pre-booking, we should be redirected to the start page.
        """
        response = self.client.get(reverse('details', args=(1234,)))
        self.assertRedirects(response, '/')

    def test_expired_pre_booking(self):
        """
        If the pre-booking has expired, we should also be redirected to the
        start page.
        """
        PreBooking.objects.create(
            session=self.client.session.session_key,
            expiration_date=datetime.now() - timedelta(minutes=1),
            ticket_type=mkTicketType(),
            quantity=1)
        response = self.client.get(reverse('details', args=(1234,)))
        self.assertRedirects(response, '/')

    def test_valid_pre_booking(self):
        """
        With a valid pre-booking, we get a 200 response (i.e. no redirection).
        """
        PreBooking.objects.create(
            session=self.client.session.session_key,
            expiration_date=datetime.now() + timedelta(minutes=1),
            ticket_type=mkTicketType(),
            quantity=1)
        response = self.client.get(reverse('details', args=(1234,)))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "details.html")


# class TicketPurchaseViewTest(TestCase):
# 
#     def test_getForm(self):
#         """
#         Test that we can get the purchase form
#         """
#         self.assertContains(self.client.get("/"), "name")
# 
# 
# class TestConfirmationView(TestCase):
# 
#     def setUp(self):
#         # It appears that client.session only work
#         # for non annonymous users: setup Test User
#         User.objects.create_user('user', 'user@site.com', 'password')
#         # Login
#         self.client.login(username='user', password='password')
# 
#         # Create data
#         tt = mkTicketType()
#         self.purchase = mkTicketPurchase(
#                 first_name="Bruce", surname="Wayne",
#                 email="bruce@wayneenterprise.com",
#                 tickets=[
#                     {'name': "Batman", 'ticket_type': tt},
#                     {'name': "Catwoman", 'ticket_type': tt},
#                 ])
#         self.invoice_id = self.purchase.invoice_id
# 
#     def test_itRedirectToTheHomePageWhenThereIsNoSessionData(self):
#         """
#         Test that /confirm/ redirect to / when the session doesn,t
#         contain any purchase data
#         """
#         self.assertRedirects(self.client.get('/confirm/'), '/')
# 
#     def test_itDisplaysTheContactName(self):
#         """
#         Test that the view displays the contact name
#         """
#         session = self.client.session
#         session['invoice_id'] = self.invoice_id
#         session.save()
#         self.assertContains(self.client.get('/confirm/'), "Bruce Wayne" )
#         self.assertContains(self.client.get('/confirm/'), "bruce@wayneenterprise.com" )
#         self.assertContains(self.client.get('/confirm/'), "bruce@wayneenterprise.com" )
# 
#     def test_itDisplaysTheTotal(self):
#         """
#         Test that the view displays the total amount
#         """
#         session = self.client.session
#         session['invoice_id'] = self.invoice_id
#         session.save()
#         self.assertContains(self.client.get('/confirm/'), "<b>Total:</b> 200 SEK" )
# 
# class TestPaypalView(TestCase):
#     def test_2(self):
#         self.client.get("/paypal/")
