from __future__ import unicode_literals

from django.contrib import admin
from django.contrib import messages

from .models import TicketPurchase, Ticket, TicketType
from .mails import (
    send_payment_confirmation_email, send_booking_confirmation_email)


# ~~~ Ticket purchase ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TicketInline(admin.TabularInline):
    """
    This is used to displayed the purchased ticket inline in the
    page showing the details of a single purchase
    """
    model = Ticket
    extra = 0


def mark_selected_paid(modeladmin, request, queryset):
    """
    Custom action that marks a bunch of tickets as paid
    using the mark_as_paid function that takes care of
    sending the cenfirmation email
    """
    for purchase in queryset:
        purchase.mark_as_paid()
mark_selected_paid.short_description = "Mark selected as paid"


def resend_booking_confirmation(modeladmin, request, queryset):
    """
    Custom admin action that re-send a confirmation email for the selected
    purchases. Note that the purchases need to be marked as paid otherwise
    the action will silently skip them
    """
    for purchase in queryset:
        send_booking_confirmation_email(purchase)
        messages.success(
            request, "Email sent to {}".format(purchase.get_contact_mailbox()))

resend_booking_confirmation.short_description = (
    "Re-send booking confirmation email")


def resend_payment_confirmation(modeladmin, request, queryset):
    """
    Custom admin action that re-send a confirmation email for the selected
    purchases. Note that the purchases need to be marked as paid otherwise
    the action will silently skip them
    """
    for purchase in queryset:
        if purchase.paid:
            send_payment_confirmation_email(purchase)
            messages.success(
                request,
                "Email sent to {}".format(purchase.get_contact_mailbox()))
        else:
            messages.warning(
                request,
                "Skiped unpaid purchase: %s. "
                "Use \"Mark selected as paid instead\"" % purchase)
resend_payment_confirmation.short_description = (
    "Re-send payment confirmation email")


def cancel_purchases(modeladmin, request, queryset):
    """
    Custom admin action that mark selected purchases as cancelled.
    """
    for purchase in queryset:
        purchase.cancel(by="ADMIN")
        purchase.save()
    messages.warning(
        request,
        "Selected purchases have been cancelled. "
        "No notification email were send, please notify the buyers")
cancel_purchases.short_description = "Cancel selected purchases"


class TicketPurchaseAdmin(admin.ModelAdmin):
    list_display = (
        'ref',
        'creation_date',
        'contact_first_name',
        'contact_surname',
        'contact_email',
        'number_of_tickets',
        'price',
        'paid', 'cancelled')
    ordering = ('-creation_date',)
    date_hierarchy = 'creation_date'
    inlines = [TicketInline]
    list_filter = ['paid', 'cancelled', 'tickets__ticket_type']
    search_fields = [
        'contact_first_name',
        'contact_surname',
        'contact_email',
        'additional_information',
        'tickets__name_on_badge']
    actions = [
        mark_selected_paid,
        cancel_purchases,
        resend_booking_confirmation,
        resend_payment_confirmation]

    def get_actions(self, request):
        """
        A bit weird but this seems to be the way to go to remove the "delete
        selected" action.
        """
        actions = super(TicketPurchaseAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions


admin.site.register(TicketPurchase, TicketPurchaseAdmin)

class TicketTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'sales_end', 'quantity')

admin.site.register(TicketType, TicketTypeAdmin)
