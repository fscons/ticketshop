from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login
from django.views.generic.base import RedirectView, TemplateView

from tickets.views import (
    index, confirmation, details,
    edit_registration, cancel_registration)
from reports.views import report


admin.autodiscover()

urlpatterns = [
    # Tickets
    url(r'^$', index, name='index'),
    url(r'^register/(\d+)$', details, name='details'),
    url(r'^r/([\w-]+)/$', confirmation, name='confirm'),
    url(r'^r/([\w-]+)/edit$', edit_registration, name='edit'),
    url(r'^r/([\w-]+)/cancel$', cancel_registration, name='cancel'),
    # Reports
    url(r'^admin/report/$', RedirectView.as_view(url='/report/')),
    url(r'^report/$', report),
    # Built-in
    url(r'404/', TemplateView.as_view(template_name='404.html')),
    url(r'^accounts/login/$', login, {'template_name': 'admin/login.html'}),
    url(r'^admin/', include(admin.site.urls)),
]
