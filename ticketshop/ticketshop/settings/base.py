"""Common settings and globals."""
import os


# ########################################################################### #
# ######## PATH CONFIGURATION
# Absolute filesystem path to the Django project directory:
DJANGO_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Absolute filesystem path to the top-level project folder:
PROJECT_ROOT = os.path.dirname(DJANGO_ROOT)

# Appsolute filesystem path to the directory where we write data
DATADIR = os.environ.get(
    'DATADIR', os.path.join(os.path.dirname(PROJECT_ROOT), 'data'))
if not os.path.exists(DATADIR):
    os.mkdir(DATADIR)
# ######## END PATH CONFIGURATION


# ########################################################################### #
# ######## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = os.environ.get('DEBUG', 'True') == 'True'
# ######## END DEBUG CONFIGURATION


# ########################################################################### #
# ######## REGISTRATION CONFIGURATION
# Time during which we hold un-finished registrations, in minutes.
if DEBUG:
    PREBOOKING_TIMEOUT = 1
else:
    PREBOOKING_TIMEOUT = 20
# ######## END REGISTRATION CONFIGURATION


# ########################################################################### #
# ######## MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Gregoire', 'gregoire@fripost.org'),
    )

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS
# ######## END MANAGER CONFIGURATION


# ########################################################################### #
# ######## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(DATADIR, 'database.sqlite'),
        },
    "test": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
        },
    }
# ######## END DATABASE CONFIGURATION


# ########################################################################### #
# ######## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-password
# EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', '')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-user
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', 'ticketshop')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-port
# EMAIL_PORT = environ.get('EMAIL_PORT', 587)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-use-tls
EMAIL_USE_TLS = False

# See: https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = 'ticketshop@fscons.org'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = "FSCONS Registrations <info@fscons.org>"
# ######## END EMAIL CONFIGURATION


# ########################################################################### #
# ######## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
# ######## END CACHE CONFIGURATION


# ########################################################################### #
# ######## GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'Europe/Stockholm'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = False
# ######## END GENERAL CONFIGURATION


# ########################################################################### #
# ######## STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = os.path.normpath(os.path.join(DATADIR, 'static'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'
# ######## END STATIC FILE CONFIGURATION


# ########################################################################### #
# ######## SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key should only be used for development and testing.
SECRET_KEY_FILE = os.path.join(DATADIR, "secret_key")
if not os.path.exists(SECRET_KEY_FILE):
    with file(SECRET_KEY_FILE, 'w') as fp:
        fp.write(os.urandom(64))

with file(SECRET_KEY_FILE, 'r') as fp:
    SECRET_KEY = fp.read()
# ######## END SECRET CONFIGURATION


# ########################################################################### #
# ######## SITE CONFIGURATION
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/1.9/ref/settings/#std:setting-ALLOWED_HOSTS
ALLOWED_HOSTS = ['tickets.fscons.org']
# ######## END SITE CONFIGURATION

# ########################################################################### #
# ######## TEMPLATE CONFIGURATION
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages"
            ],
            'debug': DEBUG,
        }
    },
]
# ######## END TEMPLATE CONFIGURATION


# ########################################################################### #
# ######## MIDDLEWARE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE_CLASSES = (
    # Default Django middleware.
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
# ######## END MIDDLEWARE CONFIGURATION


# ########################################################################### #
# ######## URL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'ticketshop.urls'
# ######## END URL CONFIGURATION


# ########################################################################### #
# ######## APP CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Admin panel and documentation:
    'django.contrib.admin',
    # Useful template tags:
    'django.contrib.humanize',

    # Local apps
    'tickets',
    'reports'
)
# ######## END APP CONFIGURATION


# ########################################################################### #
# ######## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
# ######## END LOGGING CONFIGURATION


# ########################################################################### #
# ######## WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'ticketshop.wsgi.application'
# ######## END WSGI CONFIGURATION
