from django.test import TestCase

class TestReport(TestCase):

    def test_redirect(self):
        """
        The old url /admin/report should redirect to /report
        """
        response = self.client.get('/admin/report/', follow=False)
        self.assertRedirects(response, '/report/', target_status_code=302)
