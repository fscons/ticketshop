from datetime import timedelta, date

from django.contrib.auth.decorators import permission_required
from django.shortcuts import render

from tickets.models import Ticket, TicketType, TicketPurchase
from .utils import daterange


@permission_required('tickets.view_reportk')
def report(request):
    data = {}
    tickets = Ticket.objects.all()
    tickets_ok = tickets.filter(purchase__paid=True, purchase__cancelled=False)
    tickets_unpaid = tickets.filter(purchase__paid=False, purchase__cancelled=False)

    # Total number of registrations
    data['registrations'] = {
        'ok': TicketPurchase.objects.filter(paid=True, cancelled=False).count(),
        'unpaid': TicketPurchase.objects.filter(paid=False, cancelled=False).count()
    }
    # Total number of tickets
    data['tickets'] = {
        'ok': tickets_ok.count(),
        'unpaid': tickets_unpaid.count()
    }

    # Money
    data['money'] = {}
    data['money']['ok'] = 0
    for p in TicketPurchase.objects.filter(paid=True, cancelled=False):
        data['money']['ok'] += p.price()
    data['money']['unpaid'] = 0
    for p in TicketPurchase.objects.filter(paid=False, cancelled=False):
        data['money']['unpaid'] += p.price()
    # Tickets by type
    data['bytype'] = dict(data=[], labels=[])
    for type_ in TicketType.objects.all():
        data['bytype']['labels'].append(unicode(type_))
        data['bytype']['data'].append(
            tickets_ok.filter(ticket_type=type_).count())

    # By day
    data['by_day'] = dict(labels=[], unpaid=[], cancelled=[], ok=[])
    max_day = date.today()
    min_day = max_day - timedelta(days=30)
    if min_day is not None:
        for day in daterange(min_day, max_day):
            data['by_day']['labels'].append(day.strftime("%Y-%m-%d"))
            # we want tickets created in this one day range
            drange = (day, day + timedelta(days=1))
            # counting the tickets
            count = tickets.filter(
                purchase__creation_date__range=drange,
                purchase__paid=True,
                purchase__cancelled=False
            ).count()
            data['by_day']['ok'].append(count)
            count = tickets.filter(
                purchase__creation_date__range=drange,
                purchase__cancelled=False
            ).count()
            data['by_day']['unpaid'].append(count)
            count = tickets.filter(
                purchase__creation_date__range=drange).count()
            data['by_day']['cancelled'].append(count)
    return render(request, "report.html", data)
